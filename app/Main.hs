module Main where

import qualified Data.ByteString.Lazy as BL

import qualified Data.SSP.Save as Save

main :: IO ()
main = do
  contents <- BL.readFile "C:/skyrim-alchemy/Emily - 10 days, 10 hours, 14 minutes.ess"
  save <- Save.parseSave contents

  print save
  return ()
