module Data.SSP.PluginInfo
  ( PluginInfo
  , parsePluginInfo
  ) where

import Control.Monad
import Data.Binary.Get
import Data.Text.Lazy

import Data.SSP.Internal.GetUtils

type PluginInfo = [Text]

parsePluginInfo :: Get PluginInfo
parsePluginInfo = do
  pluginCount' <- getWord8'
  plugins' <- replicateM (fromIntegral pluginCount') $ toText <$> getWString16

  return plugins'
