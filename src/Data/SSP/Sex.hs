module Data.SSP.Sex
  ( Sex(..)
  , toSex
  ) where

data Sex = Male | Female deriving (Show)

toSex :: (Num a, Eq a) => a -> Maybe Sex
toSex n
  | n == 0    = Just Male
  | n == 1    = Just Female
  | otherwise = Nothing
