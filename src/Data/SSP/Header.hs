module Data.SSP.Header
  ( Header(..)
  , parseHeader
  ) where

import Data.Binary.Get
import Data.Maybe

import qualified Data.Text.Lazy as TL

import Data.SSP.Internal.GetUtils
import Data.SSP.Sex

data Header = Header { version :: Integer
                     , saveNumber :: Integer
                     , playerName :: TL.Text
                     , playerLevel :: Integer
                     , playerLocation :: TL.Text
                     , gameDate :: TL.Text
                     , playerRaceEditorId :: TL.Text
                     , playerSex :: Sex
                     , playerCurExp :: Float
                     , playerLvlUpExp :: Float
                     , filetime :: Integer
                     , shotWidth :: Integer
                     , shotHeight :: Integer
                     } deriving (Show)

parseHeader :: Get Header
parseHeader = do
  version' <- getWord32'
  saveNumber' <- getWord32'
  playerName' <- getText
  playerLevel' <- getWord32'
  playerLocation' <- getText
  gameDate' <- getText
  playerRaceEditorId' <- getText
  -- TODO: error handling for player sex
  playerSex' <- (fromJust . toSex) <$> getWord16'
  playerCurExp' <- getFloatle
  playerLvlUpExp' <- getFloatle
  filetime' <- getWord64'
  shotWidth' <- getWord32'
  shotHeight' <- getWord32'

  return $ Header { version = version'
                  , saveNumber = saveNumber'
                  , playerName = playerName'
                  , playerLevel = playerLevel'
                  , playerLocation = playerLocation'
                  , gameDate = gameDate'
                  , playerRaceEditorId = playerRaceEditorId'
                  , playerSex = playerSex'
                  , playerCurExp = playerCurExp'
                  , playerLvlUpExp = playerLvlUpExp'
                  , filetime = filetime'
                  , shotWidth = shotWidth'
                  , shotHeight = shotHeight'
                  }
