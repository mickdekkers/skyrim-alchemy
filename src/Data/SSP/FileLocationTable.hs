module Data.SSP.FileLocationTable
  ( FileLocationTable(..)
  , parseFileLocationTable
  ) where

import Data.Binary.Get

import Data.SSP.Internal.GetUtils

data FileLocationTable = FileLocationTable { formIDArrayCountOffset :: Integer
                                           , unknownTable3Offset :: Integer
                                           , globalDataTable1Offset :: Integer
                                           , globalDataTable2Offset :: Integer
                                           , changeFormsOffset :: Integer
                                           , globalDataTable3Offset :: Integer
                                           , globalDataTable1Count :: Integer
                                           , globalDataTable2Count :: Integer
                                           , globalDataTable3Count :: Integer
                                           , changeFormCount :: Integer
                                           } deriving (Show)

parseFileLocationTable :: Get FileLocationTable
parseFileLocationTable = do
  formIDArrayCountOffset' <- getWord32'
  unknownTable3Offset' <- getWord32'
  globalDataTable1Offset' <- getWord32'
  globalDataTable2Offset' <- getWord32'
  changeFormsOffset' <- getWord32'
  globalDataTable3Offset' <- getWord32'
  globalDataTable1Count' <- getWord32'
  globalDataTable2Count' <- getWord32'
  globalDataTable3Count' <- getWord32'
  changeFormCount' <- getWord32'

  -- Skip unused
  skip $ 4 * 15

  return $ FileLocationTable { formIDArrayCountOffset = formIDArrayCountOffset'
                             , unknownTable3Offset = unknownTable3Offset'
                             , globalDataTable1Offset = globalDataTable1Offset'
                             , globalDataTable2Offset = globalDataTable2Offset'
                             , changeFormsOffset = changeFormsOffset'
                             , globalDataTable3Offset = globalDataTable3Offset'
                             , globalDataTable1Count = globalDataTable1Count'
                             , globalDataTable2Count = globalDataTable2Count'
                             , globalDataTable3Count = globalDataTable3Count'
                             , changeFormCount = changeFormCount'
                             }
