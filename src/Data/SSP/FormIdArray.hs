module Data.SSP.FormIdArray
  ( FormIdArray
  , parseFormIdArray
  ) where

import Control.Monad
import Data.Binary.Get

import Data.SSP.Internal.GetUtils

type FormIdArray = [Integer]

parseFormIdArray :: Integer -> Get FormIdArray
parseFormIdArray offset = do
  skipTo offset
  arrayCount <- getWord32'
  replicateM (fromIntegral arrayCount) getWord32'
