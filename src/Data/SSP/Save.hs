module Data.SSP.Save
  ( Save(..)
  , parseSave
  ) where

import Data.Binary.Get

import qualified Data.ByteString.Lazy as BL

import qualified Data.SSP.FormIdArray as FormIdArray
import qualified Data.SSP.Header as Header
import qualified Data.SSP.Internal.SaveHead as SaveHead
import qualified Data.SSP.Internal.SaveTail as SaveTail
import qualified Data.SSP.PluginInfo as PluginInfo

data Save = Save { header :: Header.Header
              -- , screenshot :: Image
                 , formVersion :: Integer
                 , pluginInfo :: PluginInfo.PluginInfo
                 , formIdArray :: FormIdArray.FormIdArray
                 } deriving (Show)

buildSave :: SaveHead.SaveHead -> SaveTail.SaveTail -> Save
buildSave sh st = let header' = SaveHead.header sh
                      formVersion' = SaveHead.formVersion sh
                      pluginInfo' = SaveHead.pluginInfo sh
                      formIdArray' = SaveTail.formIdArray st
                  in Save { header = header'
                          , formVersion = formVersion'
                          , pluginInfo = pluginInfo'
                          , formIdArray = formIdArray'
                          }

parseSave :: BL.ByteString -> IO Save
parseSave contents = do
  let sh = runGet SaveHead.parseSaveHead contents
  st <- SaveTail.parseSaveTail sh contents

  return $ buildSave sh st
