module Data.SSP.Internal.SaveTail
  ( SaveTail(..)
  , parseSaveTail
  ) where

import Data.Binary.Get

import qualified Data.ByteString.Lazy as BL

import qualified Data.SSP.FileLocationTable as FileLocationTable
import qualified Data.SSP.FormIdArray as FormIdArray
import qualified Data.SSP.Internal.SaveHead as SaveHead

data SaveTail = SaveTail { formIdArray :: FormIdArray.FormIdArray
                         } deriving (Show)

parseSaveTail :: SaveHead.SaveHead -> BL.ByteString -> IO SaveTail
parseSaveTail sh contents = do
  let table = SaveHead.fileLocationTable sh

  let offset = FileLocationTable.formIDArrayCountOffset table
  let formIdArray' = runGet (FormIdArray.parseFormIdArray offset) contents

  -- print formIdArray'
  return $ SaveTail { formIdArray = [] }
