module Data.SSP.Internal.SaveHead
  ( SaveHead(..)
  , parseSaveHead
  ) where

import Data.Binary.Get

import Data.SSP.Internal.GetUtils

import qualified Data.SSP.FileLocationTable as FileLocationTable
import qualified Data.SSP.Header as Header
import qualified Data.SSP.PluginInfo as PluginInfo

-- The "SaveHead" is everything up to and including the location table
data SaveHead = SaveHead { header :: Header.Header
                      -- , screenshot :: Image
                         , formVersion :: Integer
                         , pluginInfo :: PluginInfo.PluginInfo
                         , fileLocationTable :: FileLocationTable.FileLocationTable
                         } deriving (Show)


-- TODO: Error handling
parseSaveHead :: Get SaveHead
parseSaveHead = do
  -- TODO: check if magic matches
  -- Skip magic
  skip 13

  -- Skip header size
  skip 4

  -- Header
  header' <- Header.parseHeader

  -- TODO: parse screenshot
  -- Skip screenshot data
  skip $ let shotBytes = fromIntegral $ 3 * w * h
             w = Header.shotWidth header'
             h = Header.shotHeight header'
         in shotBytes

  formVersion' <- getWord8'

  -- Skip plugin info size
  skip 4

  -- Plugin info
  pluginInfo' <- PluginInfo.parsePluginInfo

  -- File location table
  fileLocationTable' <- FileLocationTable.parseFileLocationTable

  return $ SaveHead { header = header'
                 -- , screenshot = screenshot'
                    , formVersion = formVersion'
                    , pluginInfo = pluginInfo'
                    , fileLocationTable = fileLocationTable'
                    }
