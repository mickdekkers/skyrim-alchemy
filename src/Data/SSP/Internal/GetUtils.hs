module Data.SSP.Internal.GetUtils
  ( getWord8'
  , getWord16'
  , getWord32'
  , getWord64'
  , toText
  , getText
  , getWString
  , getWString8
  , getWString16
  , skipTo
  ) where

import qualified Data.ByteString.Lazy as BL
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as TL

import Data.Binary.Get

getWord8' :: Get Integer
getWord8' = fromIntegral <$> getWord8

getWord16' :: Get Integer
getWord16' = fromIntegral <$> getWord16le

getWord32' :: Get Integer
getWord32' = fromIntegral <$> getWord32le

getWord64' :: Get Integer
getWord64' = fromIntegral <$> getWord64le

-- TODO: use correct text encoding
toText :: BL.ByteString -> TL.Text
toText = TL.decodeLatin1
-- toText = Data.Encoding.CP1252.decode

getText :: Get TL.Text
getText = toText <$> getWString16

getWString :: (Integral a) => a -> Get BL.ByteString
getWString size = do getLazyByteString $ fromIntegral size

getWString8 :: Get BL.ByteString
getWString8 = do
  size <- getWord8'
  getWString size

getWString16 :: Get BL.ByteString
getWString16 = do
  size <- getWord16'
  getWString size

skipTo :: (Integral a) => a -> Get ()
skipTo a = do
  br <- fromIntegral <$> bytesRead
  skip $ fromIntegral a - br
